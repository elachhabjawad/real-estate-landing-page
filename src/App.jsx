import Header from "./components/header/Header";
import Hero from "./components/hero/Hero";
import Companies from "./components/companies/Companies";
import Residencies from "./components/residencies/Residencies";
import OurValue from "./components/our-value/OurValue";
import Contact from "./components/contact/Contact";
import GetStarted from "./components/get-started/GetStarted";
import Footer from "./components/footer/Footer";
import "./app.css";

const App = () => {
  return (
    <div className="App">
      <div>
        <div className="white-gradient"></div>
        <Header />
        <Hero />
      </div>
      <Companies />
      <Residencies />
      <OurValue />
      <Contact />
      <GetStarted />
      <Footer />
    </div>
  );
}

export default App;
