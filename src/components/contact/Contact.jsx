import { FaPhone, FaRocketchat, FaVideo, FaMessage } from "react-icons/fa6";
import "./contact.css";

const Contact = () => {
  const infosContactData = [
    { title: "Call", detail: "+18594591410", icon: <FaPhone size={25} /> },
    { title: "Chat", detail: "+13072602025", icon: <FaRocketchat size={25} /> },
    {
      title: "Video Chat",
      detail: "+15134989007",
      icon: <FaVideo size={25} />,
    },
    {
      title: "Message",
      detail: "support@homyz.com",
      icon: <FaMessage size={25} />,
    },
  ];

  return (
    <section id="contact-us" className="contact">
      <div className="paddings inner-width flex-center contact-container">
        <div className="flex-col-start contact-left">
          <span className="orange-text">Our Contact Us</span>
          <span className="primary-text">Easy to contact us</span>
          <span className="secondary-text">
            We always ready to help by providijng the best services for you. We
            beleive a good blace to live can make your life better{" "}
          </span>
          <div className="flex-col-start contact-modes">
            <div className="flex-start row">
              {infosContactData.map(({ title, detail, icon }, index) => (
                <div key={index} className="flex-col-center mode">
                  <div className="flex-start">
                    <div className="flexCenter icon">{icon}</div>
                    <div className="flex-col-start detail">
                      <span className="primary-text">{title}</span>
                      <span className="secondary-text">{detail}</span>
                    </div>
                  </div>
                  <a
                    href={
                      (detail.trim().includes("@") ? "mailto:" : "tel:") +
                      detail
                    }
                    className="flex-center button"
                  >
                    {title} now
                  </a>
                </div>
              ))}
            </div>
          </div>
        </div>
        <div className="flex-end contact-right">
          <div className="image-container">
            <img src="/images/contact.jpg" alt="contact" />
          </div>
        </div>
      </div>
    </section>
  );
};

export default Contact;
