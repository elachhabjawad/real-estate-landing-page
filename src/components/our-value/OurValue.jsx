import { useState } from "react";
import { motion } from "framer-motion";
import { Accordion, AccordionItem, AccordionItemHeading, AccordionItemButton, AccordionItemPanel, AccordionItemState} from "react-accessible-accordion";
import { MdOutlineArrowDropDown, MdCancel, MdAnalytics } from "react-icons/md";
import { HiShieldCheck } from "react-icons/hi";
import 'react-accessible-accordion/dist/fancy-example.css';
import "./our-value.css";

const OurValue = () => {

    const accordionData = [
        {
            icon: <HiShieldCheck />,
            heading: "Best interest rates on the market",
            detail:
                "Exercitation in fugiat est ut ad ea cupidatat ut in cupidatat occaecat ut occaecat consequat est minim minim esse tempor laborum consequat esse adipisicing eu reprehenderit enim.",
        },
        {
            icon: <MdCancel />,
            heading: "Prevent unstable prices",
            detail:
                "Exercitation in fugiat est ut ad ea cupidatat ut in cupidatat occaecat ut occaecat consequat est minim minim esse tempor laborum consequat esse adipisicing eu reprehenderit enim.",
        },
        {
            icon: <MdAnalytics />,
            heading: "Best price on the market",
            detail:
                "Exercitation in fugiat est ut ad ea cupidatat ut in cupidatat occaecat ut occaecat consequat est minim minim esse tempor laborum consequat esse adipisicing eu reprehenderit enim.",
        },
    ];
    const [accordionStates, setAccordionStates] = useState(Array(accordionData.length).fill(false));

    const toggleAccordion = (index) => {
        setAccordionStates(prevStates => {
            const newStates = [...prevStates];
            newStates[index] = !newStates[index];
            return newStates;
        });
    };


    return (
        <section id="our-value" className="our-value">
            <div className="paddings inner-width flex-center our-value-container">
                <div className="our-value-left">
                    <motion.div
                        initial={{ x: "7rem", opacity: 0 }}
                        animate={{ x: 0, opacity: 1 }}
                        transition={{
                            duration: 2,
                            type: "spring",
                        }}
                    className="image-container">
                        <img src="/images/value.png" alt="value" />
                    </motion.div>
                </div>
                <div className="flex-col-start our-value-right">
                    <span className="orange-text">Our Value</span>
                    <span className="primary-text">Value We Give to You</span>
                    <span className="secondary-text">We always ready to help by providijng the best services for you.<br />We beleive a good blace to live can make your life better</span>
                    <Accordion
                        className="accordion"
                        allowMultipleExpanded={false}
                        preExpanded={[0]}>
                        {accordionData.map(({heading,detail,icon}, index) =>


                            <AccordionItem key={index} className={`accordion-item ${accordionStates[index] ? 'collapsed' : ' expanded'}`} uuid={index}>
                                <AccordionItemHeading>
                                    <AccordionItemButton
                                        className="flex-center accordion-button"
                                        onClick={() => toggleAccordion(index)}>

                                        <div className="flex-center icon">{icon}</div>
                                        <span className="primary-text">
                                            {heading}
                                        </span>
                                        <div className="flex-center icon">
                                            <MdOutlineArrowDropDown size={20} />
                                        </div>
                                    </AccordionItemButton>
                                </AccordionItemHeading>
                                <AccordionItemPanel>
                                    <p className="secondary-text">{detail}</p>
                                </AccordionItemPanel>
                            </AccordionItem>
                        )}

                    </Accordion>

                </div>
            </div>
        </section>
    )
}

export default OurValue