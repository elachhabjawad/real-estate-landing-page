import { useState, useEffect, useRef } from "react";
import { BiMenuAltRight } from "react-icons/bi";
import OutsideClickHandler from "react-outside-click-handler";
import "./header.css";

const Header = () => {
    const [menuOpened, setMenuOpened] = useState(false);
    const scrolledRef = useRef(false);

    const navLinksData = [
        { title: "Residencies", path: "#residencies" },
        { title: "Our Value", path: "#our-value" },
        { title: "Contact Us", path: "#contact-us" },
        { title: "Get Started", path: "#get-started" },
        { title: "Contact", path: "/contact" },
    ];

    useEffect(() => {
        const handleScroll = () => {
            if (window.scrollY > 0) {
                scrolledRef.current.classList.add('bg-gray');
            } else {
                scrolledRef.current.classList.remove('bg-gray');
            }
        };

        window.addEventListener("scroll", handleScroll);
        return () => {
            window.removeEventListener("scroll", handleScroll);
        };
    }, []);

    const getMenuStyled = (menuOpened) => {
        if (document.documentElement.clientWidth <= 800) {
            return { right: !menuOpened && "-100%" }
        }
    };

    return (
        <section ref={scrolledRef} className="h-wrapper">
            <div className="flex-center paddings inner-width header-container">
                <img src="/images/logo.png" alt="logo" width={100} />
                <OutsideClickHandler onOutsideClick={() => setMenuOpened(false)}>
                    <ul className="flex-center header-menu" style={getMenuStyled(menuOpened)}>
                        {navLinksData.map(({ title, path }, index, arr) => arr.length - 1 !== index ?
                            (<a key={index} href={path}>{title}</a>) : (
                                <button key={index} className="button">
                                    <a href={path}>{title}</a>
                                </button>
                            )
                        )}
                    </ul>
                </OutsideClickHandler>
                <div className="menu-icon" onClick={() => setMenuOpened(c => setMenuOpened(!c))}>
                    <BiMenuAltRight size={30} />
                </div>
            </div>
        </section>



    );
};

export default Header;
