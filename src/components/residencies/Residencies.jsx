
import { Swiper, SwiperSlide, useSwiper } from 'swiper/react';
import data from "../../utils/slider.json";
import { sliderSettings } from "../../utils/common"
import 'swiper/css';
import "./residencies.css";

const Residencies = () => {
    return ( 
        <section id="residencies" className="residencies">
            <div className="paddings inner-width residencies-container">
                <div className="flex-col-start residencies-head">
                    <div className="orange-text">
                        Best Choices
                    </div>
                    <div className="primary-text">
                        Popular Residencies
                    </div>
                </div>
                <Swiper {...sliderSettings}>
                    <SliderButtons />
                    {data.map(({ image, price, name, detail }, index) =>
                        <SwiperSlide key={index} >
                            <div className="flex-col-start residencie-card">
                                <img src={image} alt={name} />
                                <span className="secondary-text">
                                    <span>{price}</span>
                                    <span style={{ color: "orange",paddingLeft:"4px" }}>$</span>
                                </span>
                                <span className="primary-text">{name}</span>
                                <span className="secondary-text">{detail}</span>
                            </div>
                        </SwiperSlide>
                    )}
                </Swiper>

            </div>

        </section>
    )
}

export default Residencies

const SliderButtons = () => {
    const slider = useSwiper();

    return (
        <div className="flex-center r-buttons">
            <button className="r-prevButton" onClick={() => slider.slidePrev()}>&lt;</button>
            <button className="r-nextButton" onClick={() => slider.slideNext()}>&gt;</button>
        </div>
    )
}