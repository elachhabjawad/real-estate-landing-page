import "./footer.css";

const Footer = () => {
  const navLinksData = [
    { title: "Property", path: "/property" },
    { title: "Services", path: "/services" },
    { title: "Product", path: "/product" },
    { title: "About Us", path: "/about-us" },
  ];
  return (
    <div className="footer">
      <div className="paddings inner-width flex-center footer-container">
        <div className="flex-col-start footer-left">
          <img src="/images/logo2.png" alt="logo2" width="120" />
          <span className="secondary-text">
            Our vision is to make all people <br />
            the best place to live for them.
          </span>
        </div>
        <div className="flex-col-start footer-right">
          <span className="primary-text">Information</span>
          <span className="secondary-text">
            2641 25th St, San Francisco, California, USA
          </span>
          <ul className="flex-center footer-menu">
            {navLinksData.map(({ title, path }, index) => (
              <a key={index} href={path}>
                {title}
              </a>
            ))}
          </ul>
        </div>
      </div>
    </div>
  );
};

export default Footer;
