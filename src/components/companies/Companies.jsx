import "./companies.css";

const Companies = () => {

    const companiesData = [
        { title: "Prologis", path: "/images/prologis.png" },
        { title: "Tower", path: "/images/tower.png" },
        { title: "Equinix", path: "/images/equinix.png" },
        { title: "Realty", path: "/images/realty.png" },
    ];

    return (
        <section id="companies" className="companies">
            <div className="paddings inner-width flex-center container-images">
                {companiesData.map(({ title, path }, index) =>
                    <img key={index} src={path} alt={title} />
                )}
            </div>
        </section>
    );
};

export default Companies;
