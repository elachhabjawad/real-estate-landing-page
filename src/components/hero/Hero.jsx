import "./hero.css";
import { HiLocationMarker } from "react-icons/hi";
import Countup from "react-countup";
import { motion } from "framer-motion";

const Hero = () => {
  const statsData = [
    { label: "Premium Product", start: 8800, end: 9000 },
    { label: "Happy Customer", start: 1950, end: 2000 },
    { label: "Awards Winning", start: 25, end: 28 },
  ];

  return (
    <section className="hero-wrapper">
      <div className="paddings inner-width flex-center hero-container">
        <div className="flex-col-start hero-left">
          <div className="hero-title">
            <div className="orange-circle"></div>

            <motion.h1
              initial={{ y: "2rem", opacity: 0 }}
              animate={{ y: 1, opacity: 1 }}
              transition={{
                duration: 2,
                type: "spring",
              }}
            >
              Discover <br />
              Most Suitable <br />
              Property
            </motion.h1>
          </div>
          <div className="flex-col-start secondary-text flexhero-des">
            <span>Find a variety of properties that suit you very easilty</span>
            <span>Forget all difficulties in finding a residence for you</span>
          </div>

          <div className="flex-center search-bar">
            <HiLocationMarker color="var(--blue)" size={25} />
            <input type="text" />
            <button className="button">Search</button>
          </div>

          <div className="flex-center stats">
            {statsData.map(({ label, start, end }, index) => (
              <div key={index} className="flex-col-center stat">
                <span>
                  <Countup start={start} end={end} duration={4} />
                  <span>+</span>
                </span>
                <span className="secondary-text">{label}</span>
              </div>
            ))}
          </div>
        </div>
        <div 
                  animate={{ x: 100 }}
        className="flex-center hero-right">
          <div className="image-container"
          >
            <img src="/images/hero-image.png" alt="hero-image" />
          </div>
              </div>
      </div>
    </section>
  );
};

export default Hero;
