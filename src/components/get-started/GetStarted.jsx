import "./get-started.css";

const GetStarted = () => {
  return (
    <div
      id="get-started"
    className="get-started">
      <div className="paddings inner-width get-started-container">
        <div className="flex-col-center inner-container">
          <span className="primary-text">Get started with Homyz</span>
          <span className="secondary-text">
            Subscribe and find super attractive price quotes from us.
            <br />
            Find your residence soon
          </span>
          <button className="button">
            <a href="mailto:support@homyz.com">Get Started</a>
          </button>
        </div>
      </div>
    </div>
  );
};

export default GetStarted;
